### HOWTO SEND ATCMD via LAN/Socket port on G4202
### 透過 LAN port 對 G420x 丟 AT COMMAND. 

G420x LAN 網路中，監聽埠 10001 可接受 socket 連線，把所需要的 AT command 直接丟入，

在 G420x 內部 Modem MGR 會把該命令插入 AT CMD queue 中，再轉發給模塊。

同樣模塊回覆的 AT RSP，Modem MGR 會再回覆給該 Socket 連線。

註：目前只支援單一連線。
 
##### Demo code:
```
#!/usr/bin/python

import socket
import time
s = socket.socket()

ip = "10.0.10.1"
port = 10001

s = socket.socket()
s.connect((ip, port))
s.send("AT")
print s.recv(1024)
s.close()
```

##### Modem MGR log:
```
2017-06-13 04:39:08,251 atcag notify inpstr: AT
2017-06-13 04:39:08,279 AT
2017-06-13 04:39:08,282 send _mgr_at_command: AT
2017-06-13 04:39:08,600 cmp cmd: AT mdm raw rsp:
2017-06-13 04:39:08,603 ['AT', 'OK']
2017-06-13 04:39:08,605 ATCAG RSP:
2017-06-13 04:39:08,607 ['OK']
```
##### In TEXT MODE - SEND SMS code:
```
s = socket.socket()
s.connect((ip, port))
s.send("AT+CMGF=1")
print s.recv(1024)
s.close()


s = socket.socket()
s.connect((ip, port))
s.send("AT+CMGS=\"+886《Number》\"")
print s.recv(1024)
s.close()

time.sleep(0.5)
s = socket.socket()
s.connect((ip, port))
s.send('TEST')
s.send(u'\u001A')
time.sleep(0.5)
print s.recv(1024)
s.close()
```
