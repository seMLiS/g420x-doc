### HOWTO USE QMI commands on G4202
####方式一，採用包裝過的 Shell script, 但選項不多，
##### 預設 APN Config file 在 /opt/mlis/conf/qmi-network.conf

命令及格式：

    /opt/mlis/qmi-network.sh [裝置] [動作]

[裝置] - /dev/cdc-wdm0, /dev/cdc-wdm0

[動作] - start/stop/status


##### Example:

建立 PS CALL

    /opt/mlis/qmi-network.sh /dev/cdc-wdm1 start

斷開 PS CALL

    /opt/mlis/qmi-network.sh /dev/cdc-wdm1 stop

狀態

    /opt/mlis/qmi-network.sh /dev/cdc-wdm1 status


####方式二，採用 qmicli，應該可以支援所有 QMI 介面：

命令及格式：

    qmicli  --help
    Usage:
      qmicli [OPTION...] - Control QMI devices
    
    Help Options:
      -h, --help                                                                            Show help options
      --help-all                                                                            Show all help options
      --help-dms                                                                            Show Device Management Service options
      --help-nas                                                                            Show Network Access Service options
      --help-wds                                                                            Show Wireless Data Service options
      --help-pbm                                                                            Show Phonebook Management options
      --help-uim                                                                            Show User Identity Module options
      --help-wda                                                                            Show Wireless Data Administrative options
    
    Application Options:
      -d, --device=[PATH]                                                                   Specify device path
      --get-service-version-info                                                            Get service version info
      --device-set-instance-id=[Instance ID]                                                Set instance ID
      --device-open-version-info                                                            Run version info check when opening device
      --device-open-sync                                                                    Run sync operation when opening device
      -p, --device-open-proxy                                                               Request to use the 'qmi-proxy' proxy
      --device-open-net=[net-802-3|net-raw-ip|net-qos-header|net-no-qos-header]             Open device with specific link protocol and QoS flags
      --client-cid=[CID]                                                                    Use the given CID, don't allocate a new one
      --client-no-release-cid                                                               Do not release the CID when exiting
      -v, --verbose                                                                         Run action with verbose logs, including the debug ones
      --silent                                                                              Run action with no logs; not even the error/warning ones
      -V, --version                                                                         Print version

##### Example:

訊號強度

    qmicli -d /dev/cdc-wdm1 --nas-get-signal-strength                                                                                                                         
    [/dev/cdc-wdm1] Successfully got signal strength
    Current:
            Network 'umts': '-89 dBm'
    Other:
            Network 'cdma-1xevdo': '-125 dBm'
    RSSI:
            Network 'umts': '-89 dBm'
            Network 'cdma-1xevdo': '-125 dBm'
    ECIO:
            Network 'umts': '-3.0 dBm'
            Network 'cdma-1xevdo': '-2.5 dBm'
    IO: '-106 dBm'
    SINR: (8) '9.0 dB'

當前使用的 profile 內容

    qmicli -d /dev/cdc-wdm1 --wds-get-profile-list=3gpp
    Profile list retrieved:
            [1] 3gpp - profile1
                    APN: 'internet'
                    PDP type: 'ipv4'
                    Auth: 'none'
