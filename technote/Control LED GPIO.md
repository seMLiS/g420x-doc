### TI AM335x GPIO address 跟 G420x LED 功能對照. ###

  待補表格

### Control from user space, ref. link: ###
1.  [Armadeus Dev board. GPIO LEDS](http://www.armadeus.com/wiki/index.php?title=GPIO_LEDS)
2.  [Define pins in DTB, ref.](http://www.crashcourse.ca/wiki/index.php/Device_tree_bindings)
3.  [Kernel source document](https://www.kernel.org/doc/Documentation/leds/leds-class.txt)

### Control GPIO output: ###
  ```
  echo 115 > /sys/class/gpio/export
  echo out > /sys/class/gpio/gpio115/direction
  echo 1 > /sys/class/gpio/gpio1115/value
  ```

4G module 驗證：

  AT+CPIN? + pppd call cdma (驗証是否能連上及是否能讀到SIM) -- NO

SIM card 由 SIM1 換至 SIM2, 重新插拔4G module,

  AT+CPIN? + pppd call cdma (驗証是否能連上及是否能讀到SIM) -- YES

### 定義LED跟實際GPIO PIN ###

```
  LED_S3=/sys/class/leds/mlb:86
  LED_S2=/sys/class/leds/mlb:87
  LED_S1=/sys/class/leds/mlb:89
  LED_4G=/sys/class/leds/mlb:117
  LED_3G=/sys/class/leds/mlb:110
  LED_FAIL=/sys/class/leds/mlb:111
  LED_READY=/sys/class/leds/mlb:112
  LED_SIM1=/sys/class/leds/mlb:113
  LED_SIM2=/sys/class/leds/mlb:114
```
