

# TI AM335x Environment requirements

### 使用 processor-sdk-02.00.02.11

http://software-dl.ti.com/processor-sw/esd/PROCESSOR-SDK-LINUX-AM335X/02_00_02_11/index_FDS.html
### 安裝流程 processor-sdk-02.00.02.11
http://processors.wiki.ti.com/index.php?title=Processor_SDK_Building_The_SDK&oldid=216985
### 安裝 cross compiler: Linaro GCC 4.9 2015.05 hard-float toolchain
```
tar -Jxvf gcc-linaro-4.9-2015.05-x86_64_arm-linux-gnueabihf.tar.xz -C /opt/
```
### 安裝 TI CCSv7
```
sudo apt-get install libc6-i386
tar -zxvf CCS7.3.0.00019_linux-x64.tar.gz -C /tmp/
./CCS7.3.0.00019_linux-x64/ccs_setup_linux64_7.3.0.00019.bin
```

勾選 Sitara AMx Proccessors，其它跟著 "Next" 即可

將 CCS workspace path 設定至 TI SDK 的 example application 目錄
```
/home/mlis/ti-processor-sdk-linux-am335x-evm-02.00.02.11/example-applications
```
目前在 G420x 上面開發的應用 ap-serial-socket, ap-reset-btn, ap-serial-config (C-based) 預設是會被放置在 這個路徑下，搭配 build image 程序。
(當然這只是"目前狀態"，希望有能人士可以幫忙改善與 CCSv7 脫勾)

# MLiS MLB-G420x build image
## G420x linux kernel source code
1. 備份 原本的 kernel, 
```
mv ~/ti-processor-sdk-linux-am335x-evm-02.00.02.11/board-support/linux-4.1.18+gitAUTOINC+bbe8cfc1da-gbbe8cfc/ ~/ti-processor-sdk-linux-am335x-evm-02.00.02.11/board-support/tisdk-linux-4.1.18+gitAUTOINC+bbe8cfc1da-gbbe8cfc/
```

2. 解壓 MLB-G420x kernel, 
```
tar zxvf mlb-linux-4.1.18+gitAUTOINC+bbe8cfc1da-gbbe8cfc.tar.gz
```
到 
```
~/ti-processor-sdk-linux-am335x-evm-02.00.02.11/board-support/tisdk-linux-4.1.18+gitAUTOINC+bbe8cfc1da-gbbe8cfc/
```

編輯 ~/ti-processor-sdk-linux-am335x-evm-02.00.02.11/local-export-config.sh
```
#!/bin/sh

export MACHINE=am335x-evm
export ARCH=arm 
export CROSS_COMPILE=arm-linux-gnueabihf- 
export PATH=/opt/gcc-linaro-4.9-2015.05-x86_64_arm-linux-gnueabihf/bin:$PATH
export PS1="\[\e[32;1m\][TISDK]\[\e[0m\]:\w> "
```

Export toolchain path. 如下：
```
. local-export-config.sh
```
便於環境變數中已設定 MACHINE=am335x-evm 、 ARCH=arm 及 CROSS_COMPILE=arm-linux-gnueabihf- ，操作 make linux kernel 時，可省略說明。

building kernel image, command list:
```
make clean
make all
make menuconfig
```

## Build MLB-G420x Application (C based.)
目前預設的 bitbucket 帳號是 bradlu, 請務必修改成自己，或直接上 bitbucket 網站拷貝網址. 

預設路徑在 /ti-processor-sdk-linux-am335x-evm-02.00.02.11/example-applications/ 下，請務必相同，因為它會和 MLB-build 裡面 build image 綁住。
(同樣地，希望往後有能人士可以把相依性問題一併解決。)

### RESET Button
```
cd ~/ti-processor-sdk-linux-am335x-evm-02.00.02.11/example-applications/
git clone https://bradlu@bitbucket.org/seMLiS/ap-reset-btn.git
```

### MCCP-Console
```
cd ~/ti-processor-sdk-linux-am335x-evm-02.00.02.11/example-applications/
git clone https://bradlu@bitbucket.org/seMLiS/ap-serial-config.git
```

### Serial-2-Socket
```
cd ~/ti-processor-sdk-linux-am335x-evm-02.00.02.11/example-applications/
git clone https://bradlu@bitbucket.org/seMLiS/ap-serial-socket.git
```

### CCSv7 加入專案
```
1. File -> Import
2. Select "Exiting Projects into Worksapce"
3. "Select root directory" is /home/mlis/ti-processor-sdk-linux-am335x-evm-02.00.02.11/example-applications/[ap-reset-btn|ap-serial-config|ap-serial-socket]
** 取消 "Search for nested projects" & "Copy projects into workspace"
4. clean project 
5. build project
```

## Build MLB-G420x image 
1. 下載 MLB-build 到 TI Process SDK 目錄下：
  目前預設的 bitbucket 帳號是 bradlu, 請務必修改成自己，或直接上 bitbucket 網站拷貝網址.
```
cd ~/ti-processor-sdk-linux-am335x-evm-02.00.02.11/
git clone https://bradlu@bitbucket.org/seMLiS/g420x-build-img.git MLB-build
```

2. 放置 MLiS Root File System (yocto built) 到 MLB-build 下
(可至 http://mlis-dev.ddns.net:9487/index.php/apps/files/?dir=%2FMLB-G420x-Release%2FDevelopment-package%2Farago-build-images 抓取 source file)
```
mkdir ~/ti-processor-sdk-linux-am335x-evm-02.00.02.11/MLB-build/targetMLB
sudo tar zxvf arago-console-image-am335x-evm-20170804073404.rootfs.tar.gz -C ~/ti-processor-sdk-linux-am335x-evm-02.00.02.11/MLB-build/targetMLB
```

3. build image
  在 build image 之前，請先完成 build  linux kernel. 
```
cd ~/ti-processor-sdk-linux-am335x-evm-02.00.02.11/ && sudo su
. local-export-config.sh && cd MLB-build/
./Buildimage.sh
```

Output help message:
```
Usage example for offical release, X/Y/N is #number.

For MLB offical release:
./Buildimage.sh --branch v100-RC1 --release v1XY-RC1 --build rN --hw MP
./Buildimage.sh --branch v100-RC1 --release v1XY-RC1 --build rN --hw PVT

For ISON offical release:
./Buildimage.sh --branch v101-RC1 --release v1XY-RC1 --build rN --hw MP
./Buildimage.sh --branch v101-RC1 --release v1XY-RC1 --build rN --hw PVT

--release, release version
--build, build-round tag
--hw, default is PVT, current diff is HW RESET BTN.
```

Example -- base on v1.0.0-RC1 branch, R3, HW is MP.
```
./Buildimage.sh --branch v100-RC1 --release v111-RC1 --build r3 --hw MP
```
Image file: mlb-rootfs-image-am335x-g420x-[YYYYMMDD]-v111-RC1-r3-MP-[hhmm].tar.gz

# MLiS MLB-G420x official image build process

1. 合併分枝 merge branch

2. 修改版號 及 build date

3. commit and push 至 remote repo.

4. build image

5. Download image to device

6. check Build date and version on Overview page.

7. 產生 FW upgrade file

8. Release Note.

   ​

